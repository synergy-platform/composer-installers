<?php

namespace Synergy\Composer;

use Composer\Package\PackageInterface;
use Composer\Installer\LibraryInstaller;

/**
 * Part of the Composer package.
 *
 * Licensed under the MIT License
 *
 * This source file is subject to the MIT License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Composer
 * @version    1.0.0
 * @author     Shane Daniels
 * @license    MIT License
 * @copyright  (c) 2015, Shane Daniels, LLC
 * @link       https://gitlab.com/synergy-platform/composer
 */

 class BaseInstaller extends LibraryInstaller
 {
     /**
      * Paths array.
      *
      * @var array
      */
     protected $paths = [
         'base'   => '/../../../..',
         'public' => '/../../../../public',
     ];

     /**
      * Returns the path.
      *
      * @param  string  $path
      * @return string
      */
     protected function getPath($path)
     {
         $pathsFile = __DIR__.'/../../../../bootstrap/paths.php';

         if (file_exists($pathsFile))  {
             $paths = require $pathsFile;
         }

         return isset($paths[$path]) ? $paths[$path] : __DIR__ . $this->paths[$path];
     }
 }
