<?php

namespace Synergy\Composer;

use Composer\Package\PackageInterface;

/**
 * Part of the Composer package.
 *
 * Licensed under the MIT License
 *
 * This source file is subject to the MIT License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Composer
 * @version    1.0.0
 * @author     Shane Daniels
 * @license    MIT License
 * @copyright  (c) 2015, Shane Daniels, LLC
 * @link       https://gitlab.com/synergy-platform/composer
 */

class ModuleInstaller extends BaseInstaller
{
    /**
     * {@inheritDoc}
     */
    public function getPackageBasePath(PackageInterface $package)
    {
        $basePath = $this->getPath('base');

        return $basePath.'/modules/'.$package->getPrettyName();
    }
    /**
     * {@inheritDoc}
     */
    public function supports($packageType)
    {
        return $packageType == 'synergy-module';
    }
}
