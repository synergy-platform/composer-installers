<?php

namespace Synergy\Composer;

use Composer\Package\PackageInterface;
use Composer\Installer\LibraryInstaller;

/**
 * Part of the Composer package.
 *
 * Licensed under the MIT License
 *
 * This source file is subject to the MIT License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Composer
 * @version    1.0.0
 * @author     Shane Daniels
 * @license    MIT License
 * @copyright  (c) 2015, Shane Daniels, LLC
 * @link       https://gitlab.com/synergy-platform/composer
 */

class Installer extends LibraryInstaller
{
    /**
     * {@inheritDoc}
     */
    public function getPackageBasePath(PackageInterface $package)
    {
        $root = $this->composer->getPackage();
        $rootExtra = $root->getExtra();
        $packageName = $package->getName();

        if (isset($rootExtra['paths'][$packageName]))  {
            return $rootExtra['paths'][$packageName];
        }

        $packageExtra = $package->getExtra();

        if (isset($packageExtra['path']))  {
            return $packageExtra['path'];
        }
    }

    /**
     * {@inheritDoc}
     */
    public function supports($packageType)
    {
        return $packageType == 'composer-installer';
    }
}
